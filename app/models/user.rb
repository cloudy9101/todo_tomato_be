class User < ApplicationRecord
  has_secure_password

  has_many :todos
  validates :email, :password, presence: true
end
